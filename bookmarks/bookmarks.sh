#!/bin/sh


USER_DATA_HOME="$XDG_DATA_HOME"
if [ -z "$USER_DATA_HOME" ]; then
	USER_DATA_HOME="$HOME/.local/share"
fi

BOOKMARK_DIR="$USER_DATA_HOME/shell-bookmarks"

function error()
{
	echo "$@" >&2
}

function add()
{
	NAME="$1"
	if [ -z "$NAME" ]; then
		error 'No Bookmark name given'
		exit 2
	fi
	DEST="$2"
	if [ -z "$DEST" ]; then
		DEST="$PWD"
	fi
	DEST=$(realpath -s "$DEST")
	mkdir --parents "$BOOKMARK_DIR"
	echo "$DEST" > "$BOOKMARK_DIR/$NAME"
	echo "Added Bookmark \"$NAME\"->\"$DEST\""
}

function get()
{
	NAME="$1"
	BOOKMARK_PATH="$BOOKMARK_DIR/$NAME"
	if ! [ -f "$BOOKMARK_PATH" ]; then
		error "Bookmark $NAME not found"
		exit 1
	fi
	cat "$BOOKMARK_PATH"
}

function delete()
{
	NAME="$1"
	BOOKMARK_PATH="$BOOKMARK_DIR/$NAME"
	if ! [ -f "$BOOKMARK_PATH" ]; then
		error "Bookmark $NAME not found!"
		exit 1
	fi
	rm "$BOOKMARK_PATH"
}

function usage()
{
	echo -n "$0 "
	cat - << EOF
<add, delete, get> [destination-dir]
<listBookmarks>
EOF
}

function listBookmarks()
{
	for b in "$BOOKMARK_DIR"/*; do
		if [ "$(basename $b)" = '*' ]; then
			break
		fi
		echo "$(basename $b)"
	done
}

case "$1" in
	add)
		shift
		add "$@"
		;;
	delete)
		shift
		delete "$@"
		;;
	get)
		shift
		get "$@"
		;;
	listBookmarks)
		shift
		listBookmarks "$@"
		;;
	*)
		error "Unknown Command: " "$1"
		usage "$@"
		exit -1
		;;
esac
