#This File is intented to be sourced

function goto_bookmark()
{
	local BOOKMARK_PATH=$(bookmarks get "$@")
	if [ -n "$BOOKMARK_PATH" ]; then
		echo 'Entering ' "\"$BOOKMARK_PATH\""
		cd "$BOOKMARK_PATH"
	fi
}

function _bookmark_completion() {
	# Was no action specified?
	if [ "${#COMP_WORDS[@]}" = 2 ]; then 
		COMPREPLY=($(compgen -W "add delete listBookmarks get" "${COMP_WORDS[1]}"))
	elif [ "${#COMP_WORDS[@]}" = 3 ] && [ "${COMP_WORDS[1]}" = "get" -o "${COMP_WORDS[1]}" = "delete" ]; then
		COMPREPLY=($(compgen -W "$(bookmarks listBookmarks)" "${COMP_WORDS[2]}"))
	fi
}


function _goto_bookmark_completion() {
	if [ "${#COMP_WORDS[@]}" = 2 ]; then 
		COMPREPLY=($(compgen -W "$(bookmarks listBookmarks)" "${COMP_WORDS[1]}"))
	fi
}

complete -F _goto_bookmark_completion goto_bookmark
complete -F _bookmark_completion bookmarks
