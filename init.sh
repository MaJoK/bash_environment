# Abort when started in sh
# Some alias names are not compatible with sh
if [ "$(ps -p $$ -ocomm=)" = "sh" ]; then
	return
fi
script_path="$(realpath "${BASH_SOURCE[0]}")"
dir="$(dirname "$script_path")"
export PATH="$PATH:$dir/bin"

source "$dir/aliases.sh"
source "$dir/functions.sh"
source "$dir/bookmarks/bookmarks_profile.sh"

export EDITOR=vim
export VISUAL=$EDITOR

export HISTFILESIZE=50000
export HISTSIZE=50000
export HISTFILE=~/.history
DEFAULT_PROMPT_COMMAND="$PROMPT_COMMAND"
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
export HISTCONTROL=erasedups
export HISTTIMEFORMAT="[%F %T] "

pwd_for_ps1() {
	local path="$(dirs +0)"
	local max_path_length=${PS1_PATH_LENGTH:-80}
	if [ ${#path} -gt $max_path_length ]; then
		local cut_size=$(expr $max_path_length / 2)
		local pre_part="$(echo ""$path"" | cut -c 1-$cut_size)"
		local post_part_begin=$(expr ${#path} - $cut_size )
		local post_part="$(echo ""$path"" | cut -c $post_part_begin-${#path})"
		path="$pre_part...$post_part"
	fi
	echo -n "$path"
}

command_prefix() {
    if [ "$(id -u)" = 0 ]; then
        echo -n '#'
    else
        echo -n '$'
    fi
}

init_ps1() {
	local red_bold='\[\e[1;31m\]'
	local yellow_bold='\[\e[1;33m\]'
	local default='\[\e[0;m\]'
	PS1=""
	if [ "a$PS1_SHOW_USER" != "a" -a "a$PS1_SHOW_USER" != "a0" ]; then
		PS1="$PS1\u@$red_bold\h:"
	fi
    local clock_text="t"
    if [[ "$PS1_NO_EMOJI" = 0 || "$PS1_NO_EMOJI" = "" ]]; then
        clock_text='🕔'
    fi      
    PS1="$PS1$yellow_bold"'$(pwd_for_ps1)'"$default\n""$clock_text"'$(date +%R:%S)'"|!\!"
    PS1="${PS1}$(command_prefix) "
}

is_opensuse() {
	test -f /etc/SUSE-brand
}

disable-history() {
    echo Disabling History...
    export HISTFILE=/dev/null
    PROMPT_COMMAND="$DEFAULT_PROMPT_COMMAND"
}

init_ps1
[ -f ~/.bash_functions ] && source ~/.bash_functions
[ -f ~/.bash_aliases ] && source ~/.bash_aliases
is_opensuse && source "$dir/opensuse.sh"

#Case insensitive tab-completion
bind 'set completion-ignore-case on'
