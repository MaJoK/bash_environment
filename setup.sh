#!/bin/bash
script_path="$(realpath -s ${BASH_SOURCE[0]})"
dir="$(dirname $script_path)"

if [ -f ~/.bashrc ]; then
	
	# Does it already contain the source line?
	SOURCE_LINE="source \"$dir/init.sh\""
	if ! grep -F "$SOURCE_LINE" ~/.bashrc >/dev/null; then
		echo 'Line to source init.sh not found in /.bashrc, adding it'
		echo "$SOURCE_LINE" >> ~/.bashrc
	fi
else
	echo Couldn\'t find '~/.bashrc'
fi
# Source line for .vimrc
SOURCE_LINE="source $dir/vimrc"
VIM_NEEDS_REPO_PLUGIN=0
VIM_PLUGIN="$HOME/.vim/plugin/repo_vim_config.vim"
if [ -f "$VIM_PLUGIN" ]; then
	VIM_NEEDS_REPO_PLUGIN=0
else
	VIM_NEEDS_REPO_PLUGIN=1
fi
if [ $VIM_NEEDS_REPO_PLUGIN = 1 ]; then
	echo 'Plugin to load config.vim not found, creating the symlink'
	mkdir --parents -v ~/.vim/plugin
	ln -sv "$dir/config.vim" "$VIM_PLUGIN"
fi

starship_toml="$HOME/.config/starship.toml"
if [ -h "$starship_toml" ] && [ "$(readlink $starship_toml)" = "$dir/starship.toml" ]; then
	echo 'legacy starship.toml is present removing it'
	rm -v "$starship_toml"
fi

GITCONFIG="$dir/gitconfig"
GITCONF_LINE="	path = \"$GITCONFIG\""
if ! grep -F "$GITCONF_LINE" ~/.gitconfig > /dev/null; then
	echo 'Adding line to include gitconfig to ~/.gitconfig'
	echo '[include]' >> ~/.gitconfig
	echo "$GITCONF_LINE" >> ~/.gitconfig
fi

echo 'End of setup'
