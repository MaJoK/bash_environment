function up_dir()
{
	local count="$1"
	if [ a"$count" = a ]; then
		count=1
	fi
	for (( i=0; i < $count; i++)) ; do
		cd ..
	done
}

csvndiff()
{
	svn diff "$@"|colordiff|less -r
}
