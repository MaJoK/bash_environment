#!/usr/bin/env bash

API_URL='https://api.telegram.org/'
error() { echo >&2 "$@" ;}
is_installed() { command -v "$1" >/dev/null 2>&1 ;}






usage() {
	echo
	echo 'Usage:'
	echo "$0: [options] <chat-id> <message-text>"
	echo
	echo 'Send messages to telegram chats'
	echo
	echo 'Options:'
	echo -e ' -t, --token <token>  set the bot-token that is used for the request, mandatory when BOT_TOKEN is not set'
	echo -e ' -v, --verbose        verbose output'
	echo
	echo 'Environment:'
	echo -e ' BOT_TOKEN            The bot-token that is used for the request, required when the token option is not used'
}

if [ $# = 0 ]; then
	error "$0: no arguments given"
	usage
	exit 4;
fi

! PARSED=$(getopt -o t:v -l token:,verbose --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi

eval set -- "$PARSED"

token="$BOT_TOKEN"
verbose=n

if ! is_installed curl; then
	error "curl is required but couldn't be found on the path"
	exit 5
fi
while true; do
	case $1 in
		-t|--token)
			token="$2"
			shift 2
			;;

		-v|--verbose)
			verbose=y
			shift
			;;
		--)
			shift
			break
			;;
		*)
			error "Programming error"
			exit 3
			;;
	esac
done
if [ a"$token" = a ]; then
	error "No token given with the BOT_TOKEN environment-variable, -t or --token option"
	exit 4
fi
if [ ! "$#" -ge 2 ]; then
	error "$0: Target chat-id and message text expected"
	exit 4
fi

verbose_echo() {
	if [ $verbose = y ]; then
		echo "$@"
	fi
}

chat_id="$1"
verbose_echo "chat_id is $chat_id"
shift;
message="$*"
verbose_echo "Message is '$message'"
curl_args=-s
if [ $verbose = y ]; then
	curl_args="$curl_args -v"
fi
call_url="$API_URL""bot$token/sendMessage?chat_id=$chat_id"
verbose_echo "Calling curl on URL: $call_url"

if [ $verbose = y ]; then
	curl --data "text=$message" $curl_args "$call_url"
else
	curl --data "text=$message" $curl_args "$call_url" > /dev/null
fi
