#!/usr/bin/env bash

# Script to easily generate systemd Unit files to periodically start a program
set -e

UNIT_NAME="$1"
UNIT_DESC="$2"
EXEC_PATH="$3"
function error(){ echo "$@" >&2; }

if [[ "$1" = "" || "$2" = "" || "$3" = "" ]]; then
    error "Usage: $0: <unit_name> <unit_description> <path_to_exec>"
    exit 1
fi

# Resolve to absolute path
EXEC_PATH="$(realpath ""$EXEC_PATH"")"


SERVICE_FILE="/etc/systemd/system/$UNIT_NAME.service"
TIMER_FILE="/etc/systemd/system/$UNIT_NAME.timer"
echo Creating files "$SERVICE_FILE" and "$TIMER_FILE"

if [[ -f "$SERVICE_FILE" || -f "$TIMER_FILE" ]]; then
    error "At least one of the files exists already"
    exit 1
fi

sudo tee "$SERVICE_FILE" <<DOCS
[Unit]
Description=$UNIT_DESC

[Service]
ExecStart=$EXEC_PATH
DOCS

sudo tee "$TIMER_FILE" <<DOCS
[Unit]
Description=$UNIT_DESC

[Timer]
OnCalendar=*-*-* *:00:00
RandomizedDelaySec=60

[Install]
WantedBy=multi-user.target
DOCS

sudo systemctl enable --now "$UNIT_NAME.timer"
