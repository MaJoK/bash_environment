#!/bin/bash
vid_length() {
	ffmpeg -i "$1" 2>&1 | grep "Duration"| cut -d ' ' -f 4 | sed s/,// | sed 's@\..*@@g' | awk '{ split($1, A, ":"); split(A[3], B, "."); print 3600*A[1] + 60*A[2] + B[1] }'
}

totalLength=0
for i in "$@"; do
    len="$(vid_length "$i")"
    totalLength="$(( "$len" + "$totalLength" ))"
done
echo "$(( "$totalLength" / 60 )) min"
