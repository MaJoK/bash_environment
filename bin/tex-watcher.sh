#!/bin/sh
if [ "x$1" = x ]; then
	echo 'No tex file given' >&2
	exit 1
fi
OLD_CHANGE_TIMES=""
while true; do
	CHANGE_TIMES="$(stat -c %Y ./*.tex)";
	if [ "$CHANGE_TIMES" != "$OLD_CHANGE_TIMES" ]; then
		latexmk -pdf -synctex=1 -interaction=nonstopmode "$1"
        if [ $? != 0 ]; then
	        red_bold='\e[1;31m'
        	default='\e[0;m'
            echo -e "$red_bold" '-------- PDF Error --------' "$default"
        fi
	else
		sleep 0.25;
	fi
	OLD_CHANGE_TIMES="$CHANGE_TIMES";
done
