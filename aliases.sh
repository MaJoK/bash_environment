alias clipboard="xclip -selection clipboard"
alias ls='ls -lAh --color=auto'
alias ..='up_dir'

alias pwd_to_clipboard='echo -n "$PWD" | clipboard'
alias ba='bookmarks add'
alias b='goto_bookmark'
complete -F _goto_bookmark_completion b

alias :q='exit'

alias full-guix-update='guix pull && guix upgrade && guix gc'
