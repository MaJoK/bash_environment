# This is file is intended to be sourced

function full-system-update() {
	sudo screen sh -c 'zypper dup --no-allow-vendor-change -l && flatpak --system update -y && flatpak --system uninstall --unused'
	flatpak --user update -y && flatpak --user uninstall --unused
}

function system-update() {
	sudo screen sh -c 'zypper dup --no-allow-vendor-change -l && flatpak --system update -y'
	flatpak --user update -y
}
