# Configuration
## Environment Variables
 + PS1\_PATH\_LENGTH

 Controls what's the maximum length the Path shown in the PS1 should have.  
 When it becomes longer parts in the middle will be cut out and a "..." will be shown.
 
 Defaults to 50

 + PS1\_SHOW\_USER

 When not empty or 0 the current user is shown in the ps1
